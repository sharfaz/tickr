<?php

namespace App\APIResource;

use ApiPlatform\Core\Annotation\ApiProperty;
use DateTimeImmutable;

class CarbonOffsetSchedule
{
    /**
     * @ApiProperty(identifier=true)
     * @var array|DateTimeImmutable[]
     */
    private array $dates = [];

    /**
     * @return array
     */
    public function getDates(): array
    {
        return $this->dates;
    }

    /**
     * @param array|DateTimeImmutable[] $dates
     */
    public function setDates(array $dates): void
    {
        $this->dates = $dates;
    }

}