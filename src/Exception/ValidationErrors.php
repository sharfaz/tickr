<?php

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class ValidationErrors
{
    /**
     * @var ConstraintViolationList
     */
    private ConstraintViolationList $constraintViolations;

    public function __construct()
    {
        $this->constraintViolations = new ConstraintViolationList();
    }

    /**
     * Adds violation errors constraint violation list
     *
     * @param string $message
     * @param string|null $propertyPath
     */
    public function addViolation(string $message, ?string $propertyPath = ''): void
    {
        $this->constraintViolations->add(new ConstraintViolation($message,
            '',
            [],
            null,
            $propertyPath,
            null
        ));
    }

    /**
     * Returns violation list count
     *
     * @return int
     */
    public function count(): int
    {
        return $this->constraintViolations->count();
    }

    /**
     * Gets violation errors
     *
     * @return ConstraintViolationList|null
     */
    public function getViolations(): ?ConstraintViolationList
    {
        return $this->constraintViolations;
    }
}