<?php

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\APIResource\CarbonOffsetSchedule;
use App\Exception\ValidationErrors;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class CarbonOffsetDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var ValidationErrors
     */
    private ValidationErrors $validationErrors;

    public function __construct(ValidationErrors $validationErrors)
    {
        $this->validationErrors = $validationErrors;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        $subscriptionDate = null;
        $scheduleMonths = null;

        if (!isset($context['filters'])) {
            $this->validationErrors->addViolation('Required query parameters subscriptionStartDate and scheduleInMonths are missing');
        }

        if (isset($context['filters']) && !isset($context['filters']['subscriptionStartDate'])) {
            $this->validationErrors->addViolation('Query parameter subscriptionStartDate missing', 'subscriptionStartDate');
        }

        if (isset($context['filters']) && !isset($context['filters']['scheduleInMonths'])) {
            $this->validationErrors->addViolation('Query parameter scheduleInMonths missing', 'scheduleInMonths');
        }

        if (isset($context['filters']['subscriptionStartDate'])) {
            $subscriptionDate = $context['filters']['subscriptionStartDate'];

            try {
                $subscriptionDate = Carbon::createFromFormat('Y-m-d', $subscriptionDate);
                if ($subscriptionDate->isFuture()) {
                    $this->validationErrors->addViolation('Date must be current or past date', 'subscriptionStartDate');
                }
            } catch (\Exception $exception) {
                $this->validationErrors->addViolation('Invalid date format', 'subscriptionStartDate');
            }
        }

        if (isset($context['filters']['scheduleInMonths'])) {
            $scheduleMonths = (int) $context['filters']['scheduleInMonths'];
            if (is_null($scheduleMonths) || !is_int($scheduleMonths)) {
                $this->validationErrors->addViolation('Invalid value provided', 'scheduleInMonths');
            }

            if ($scheduleMonths < 0 || $scheduleMonths > 36) {
                $this->validationErrors->addViolation('Schedule in months value must be number between 0-36', 'scheduleInMonths');
            }
        }

        if ($this->validationErrors->count() > 0) {
            throw new ValidationException($this->validationErrors->getViolations());
        }

        // return no results if month input is 0
        if ($scheduleMonths === 0) {
            return [];
        }

        $startDate = $subscriptionDate->addMonthNoOverflow();


        $endDate = $startDate->copy()->addMonths($scheduleMonths)->subMonth();
        $period = CarbonPeriod::since($startDate)->month(1)->until($endDate);

        $carbonOffset = new CarbonOffsetSchedule();
        $dates = [];
        foreach ($period as $key => $date) {
            $dates[] = $date->format('Y-m-d');
        }
        $carbonOffset->setDates($dates);

        return $carbonOffset->getDates();
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === CarbonOffsetSchedule::class;
    }
}