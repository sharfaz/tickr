# Tickr Carbon Offsetting Challenge 

### Task completed by Mohamed Sharfaz Sairaz

The task is completed using Symfony 5.2 release with API Platform and PHP 7.4. Please follow the steps below to run the project in your environment.

- Clone the repository
- Run `composer install`
- If you are using local PHP server `php -S 127.0.0.1:8000 -t public`
